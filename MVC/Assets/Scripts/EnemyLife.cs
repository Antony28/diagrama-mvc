﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLife : MonoBehaviour
{

    public int initialLife;
    public int currentLife;

    public int playerDmg;

    void Start()
    {
        currentLife = initialLife;
    }

    void Update()
    {
        if(currentLife <= 0)
        {
            Die();
        }
    }

    public void TakeDamage()
    {
        currentLife -= playerDmg; 
    }

    void Die()
    {
        Debug.Log("Enemy Die");
        Destroy(gameObject);
    }
}
