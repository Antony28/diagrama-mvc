﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVista : MonoBehaviour
{
    public PlayerModelo model;

    public void SetLifeText()
    {
        model.lifeText.text = "Vidas: " + model.currentLife;
        if (model.currentLife <= 0)
        {
            Die();
        }
    }
    public void Die()
    {
        Destroy(gameObject);
    }
    public void TakeDamage()
    {
        model.currentLife -= model.enemyDmg;
    }
}
