﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPlayer : MonoBehaviour
{
    PlayerVista pVista;

    void Start()
    {
        pVista = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerVista>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            pVista.TakeDamage();
            pVista.SetLifeText();
        }
    }
}
